/*
*(c) Copyright 2016 Dennis Araya. Some Rights Reserved. 
*This work is licensed under a Creative Commons Attribution-Noncommercial-Share Alike 3.0 License
*/

$(
	function(){
		$( document ).keydown(
			function ( event ) { 
				Typer.addText( event ); //Captura el keydown y ejecuta el addtext, esto se hace al cargar el sitio
			}
		);
	}
);

var Typer={
	text: null,
	accessCountimer:null,
	index:0, // posición actual del cursor
	speed:2, // rapidez del typer
	file:"", //file debe estar seteado
	accessCount:0, //veces en que se presiona alt para acceso otorgado
	deniedCount:0, //veces en que se presiona alt para acceso denegado
	init: function(){// inicialización
		accessCountimer=setInterval(function(){Typer.updLstChr();},500); // inicializa timer de parpadeo de cursor
		$.get(Typer.file,function(data){// obtiene el archivo de texto
			if(data==='Texto')
				console.log('Invalido');
			else
				Typer.text=data;// guarda el texto
		});
	},
	
	content:function(){
		return $("#console").html();// obtiene el contenido de consola
	},
	
	write:function(str){// agrega el contenido a la consola
		$("#console").append(str);
		return false;
	},
	
	makeAccess:function(){//muestra el popup de acceso concedido. Este no se scrollea por ahora....
		Typer.hidepop(); // oculta los popups
		Typer.accessCount=0; //reinicia el conteo
		var ddiv=$("<div id='gran'>").html(""); // crea un nuevo div en blanco
		ddiv.addClass("accessGranted"); // agrega clase al div
		ddiv.html("<h1>ACCESS GRANTED</h1>"); // setea contenido al div
		$(document.body).prepend(ddiv); // lo agrega al body
		return false;
	},
	makeDenied:function(){//crea el acceso denegado. Tampoco se scrollea por ahora ...
		Typer.hidepop(); // oculta los popups
		Typer.deniedCount=0; //resetea contador
		var ddiv=$("<div id='deni'>").html(""); // crea el nuevo div
		ddiv.addClass("accessDenied");// agrega clase al div
		ddiv.html("<h1>ACCESS DENIED</h1>");// setea contenido al div
		$(document.body).prepend(ddiv);// lo agrega al body
		return false;
	},
	
	hidepop:function(){// quita todos los popups
		$("#deni").remove();
		$("#gran").remove();
	},
	
	addText:function(key){//función principal para agregar código
		if(key.keyCode==18){// key 18 = alt key
			Typer.accessCount++; //aumenta contador
			if(Typer.accessCount>=3){// si se presiona 3 veces
				Typer.makeAccess(); // hace acceso al popup
			}
		}else if(key.keyCode==20){// key 20 = caps lock
			Typer.deniedCount++; // aumenta contador
			if(Typer.deniedCount>=3){ // 
				Typer.makeDenied(); // 
			}
		}else if(key.keyCode==27){ // key 27 = esc key
			Typer.hidepop(); // 
		}else if(Typer.text){ // 
			var cont=Typer.content(); // 
			if(cont.substring(cont.length-1,cont.length)=="|") // 
				$("#console").html($("#console").html().substring(0,cont.length-1)); // 
			if(key.keyCode!=8){ // 
				Typer.index+=Typer.speed;	// 
			}else{
				if(Typer.index>0) // 
					Typer.index-=Typer.speed;//	
			}
			var text=$("<div/>").text(Typer.text.substring(0,Typer.index)).html();// 
			var rtn= new RegExp("\n", "g"); // 
			var rts= new RegExp("\\s", "g"); // 
			var rtt= new RegExp("\\t", "g"); // 
			$("#console").html(text.replace(rtn,"<br/>").replace(rtt,"&nbsp;&nbsp;&nbsp;&nbsp;").replace(rts,"&nbsp;"));// 
			window.scrollBy(0,50); // 
		}
		if ( key.preventDefault && key.keyCode != 122 ) { //
			key.preventDefault()
		};  
		if(key.keyCode != 122){ // 
			key.returnValue = false;
		}
	},
	
	updLstChr:function(){ // 
		var cont=this.content(); // 
		if(cont.substring(cont.length-1,cont.length)=="|") // 
			$("#console").html($("#console").html().substring(0,cont.length-1)); // 
		else
			this.write("|"); // 
	}
}